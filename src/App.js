import Todo from "./Todo"
import { useState } from "react"
import TodoForm from './TodoForm';
import { todos } from "./todos"

function App() {
  const [arrayTodo, setArrayTodo] = useState(todos);

  const handleDeleteClick = e => {
    const newTodos = arrayTodo.filter(item => item.id !== e.target.id);
    setArrayTodo(newTodos);
  };

  const handleAddTodo = text => {
    const newTodos = [
      ...arrayTodo,
      {
        id: new Date().getTime().toString(),
        text: text,
      },
    ];
    setArrayTodo(newTodos);
  };
  return (
    <div className="container">
      {arrayTodo.map(todo => (
        <Todo
          key={todo.id}
          id={todo.id}
          text={todo.text}
          onTodoClick={handleDeleteClick}
        />
      ))}
      <TodoForm onAddTodo={handleAddTodo} />
    </div>
  );
}

export default App;
