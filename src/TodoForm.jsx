import { useState } from 'react'

export default function TodoForm(props) {
    const [inputTodo, setInputTodo] = useState('');
    const [pesanErrors, setpesanErrors] = useState(null);
    const handleInputChange = e => {
        setInputTodo(e.target.value);
    };

    const handleFormSubmit = e => {
        e.preventDefault();
        if (inputTodo.trim() === '') {
            setpesanErrors('Todo tidak boleh kosong');
        } else {
            props.onAddTodo(inputTodo);
            setInputTodo('');
        }
    };

    return (
        <form onSubmit={handleFormSubmit}>
            <div>
                <input
                    type="text"
                    placeholder="Add todo..."
                    value={inputTodo}
                    onChange={handleInputChange}
                /> <br />
                {pesanErrors && <small>{pesanErrors}</small>}
            </div>
        </form>
    );
}
